# HTTSERVER

Escriba en python un pequeño webserver que funcione como un Apache cuando se habilita el directory listing.
Cuando se accede desde un browser debe mostrar la estructura de archivos y directorios (desde el directorio actual donde se ejecutó) y permitir navegar por los mismos.
Solo se debe usar la standard library de python y no se puede usar el modulo http.

## como ejecutar
`python httpserver `
or
`python httpserver 3000`

##escrito
Manuelen12@gmail.com